defmodule Namecheap do
  @moduledoc """
  Simple functions for interacting with the [Namecheap API][1].

  [1]: https://www.namecheap.com/support/api/intro/
  """
  alias Namecheap.Client

  @type params :: %{String.t() => String.t()}

  @doc """
  Check the status of domain names, including their price and availability.

  Pass a list of domain names and receive a list of
  `Namecheap.DomainCheckResult` structs.

  The order of the results is undefined.

  ## Example

  ```elixir
  iex(1)> Namecheap.check_domains(["tribes.host", "us.xyz"])
  {:ok,
   [
     %Namecheap.DomainCheckResult{
       available: false,
       description: "",
       domain: "tribes.host",
       eap_fee: #Decimal<0>,
       error_no: "0",
       icann_fee: #Decimal<0>,
       is_premium: false,
       premium_registration_price: #Decimal<0>,
       premium_renewal_price: #Decimal<0>,
       premium_restore_price: #Decimal<0>,
       premium_transfer_price: #Decimal<0>
     },
     %Namecheap.DomainCheckResult{
       available: true,
       description: "",
       domain: "us.xyz",
       eap_fee: #Decimal<0.0>,
       error_no: "0",
       icann_fee: #Decimal<0>,
       is_premium: true,
       premium_registration_price: #Decimal<13000.0>,
       premium_renewal_price: #Decimal<13000.0>,
       premium_restore_price: #Decimal<65.0>,
       premium_transfer_price: #Decimal<13000.0>
     }
   ]}
  ```
  """
  @spec check_domains(domains :: [String.t()]) ::
          {:ok, [Namecheap.DomainCheckResult.t()]} | {:error, any()}
  def check_domains(domains) when is_list(domains) do
    params = %{"DomainList" => Enum.join(domains, ",")}
    do_command("namecheap.domains.check", params)
  end

  @doc """
  Register a domain name for a `Namecheap.Contact`.
  It returns a `Namecheap.DomainCreateResult` struct.

  Namecheap requires you to send four different contacts:
  Registrant user, Tech user, Admin user, and AuxBilling user.
  This function assumes they are all the same.

  Not all fields of `Namecheap.Contact` are required.
  Take a look at [the API docs][1] to see which fields are needed.

  ## Extra rules

  - The `extra_params` argument is merged over the request params last, and can
    be used to add/override any params. It is a map of literal PascalCase
    Namecheap params with string keys.

  - If your domain name ends with `.us`, `.eu`, `.ca`, `.co.uk`, `.org.uk`,
    `.me.uk`, `.nu`, `.com.au`, `.net.au`, `.org.au`, `.es`, `.nom.es`,
    `.com.es`, `.org.es`, `.de`, or `.fr`, you will need to send
    [Extended Attributes][2] with the `extra_params` argument, otherwise
    the request will fail.

  - This function is untested with non-ASCII domain names. Namecheap hints that
    you need to pass the `"IdnCode"` parameter with `extra_params`.

  - This function enables [WhoisGuard][3] for the domain by default.
    You may disable it by adding `%{"WGEnabled" => "no"}`
    to `extra_params`.

  ## Example

  ```elixir
  iex(1)> contact = %Namecheap.Contact{first_name: "Herman", last_name: "Munster", email: "herman@munster.me", phone: "+001.1234567890", address_line_1: "1313 Mockingbird Lane", city: "Mockingbird Heights", state_or_province: "NY", postal_code: "12200", country: "United States"}
  iex(2)> Namecheap.register_domain("tribes.host", 1, contact)
  {
    :ok,
    %Namecheap.DomainCreateResult{
      charged_amount: #Decimal<170.1200>,
      domain: "tribes.host",
      domain_id: "670288",
      order_id: "2332723",
      realtime: true,
      registered: true,
      transaction_id: "5333932",
      whois_guard_enabled: true
    }
  }
  ```

  [1]: https://www.namecheap.com/support/api/methods/domains/create/
  [2]: https://www.namecheap.com/support/api/extended-attributes/
  [3]: https://www.namecheap.com/security/whoisguard/
  """
  @spec register_domain(
          domain :: String.t(),
          years :: integer(),
          Namecheap.Contact.t(),
          extra_params :: Namecheap.params()
        ) :: {:ok, any()} | {:error, any()}
  def register_domain(domain, years, %Namecheap.Contact{} = contact, extra_params \\ %{})
      when is_binary(domain) and is_integer(years) and is_map(extra_params) do
    params =
      contact
      |> Namecheap.Contact.to_params(["Registrant", "Tech", "Admin", "AuxBilling"])
      |> Map.put("DomainName", domain)
      |> Map.put("Years", years)
      |> Map.put("AddFreeWhoisguard", "yes")
      |> Map.put("WGEnabled", "yes")
      |> Map.merge(extra_params)

    case do_command("namecheap.domains.create", params) do
      {:ok, [%Namecheap.DomainCreateResult{} = result | _]} -> {:ok, result}
    end
  end

  defp do_command(cmd, params) do
    with {:ok, %Namecheap.Response{status: "OK", body: body}} <- Client.command(cmd, params) do
      {:ok, body}
    else
      {:ok, %Namecheap.Response{} = response} -> {:error, response}
    end
  end
end
