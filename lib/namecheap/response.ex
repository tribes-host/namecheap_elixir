defmodule Namecheap.Response do
  @moduledoc """
  Struct representation of an API response returned by Namecheap.

  ```elixir
  %Namecheap.Response{
    body: [
      %Namecheap.DomainCheckResult{
        available: true,
        description: "",
        domain: "tribes.host",
        eap_fee: #Decimal<0>,
        error_no: "0",
        icann_fee: #Decimal<0>,
        is_premium: false,
        premium_registration_price: #Decimal<0>,
        premium_renewal_price: #Decimal<0>,
        premium_restore_price: #Decimal<0>,
        premium_transfer_price: #Decimal<0>
      }
    ],
    command: "namecheap.domains.check",
    errors: [],
    execution_time: #Decimal<1.302>,
    gmt_time_difference: "--5:00",
    server: "PHX01SBAPIEXT06",
    status: "OK",
    warnings: []
  }
  ```
  """
  @type t :: %__MODULE__{
          status: String.t(),
          errors: list(),
          warnings: list(),
          command: String.t(),
          server: String.t(),
          gmt_time_difference: String.t(),
          execution_time: Decimal.t(),
          body: list() | map()
        }

  defstruct status: "",
            errors: [],
            warnings: [],
            command: "",
            server: "",
            gmt_time_difference: "",
            execution_time: Decimal.new(0),
            body: nil
end
