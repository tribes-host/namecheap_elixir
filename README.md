# Namecheap Elixir

Elixir client library for interacting with the [Namecheap API](https://www.namecheap.com/support/api/intro/).

The goal of this library is to facilitate the creation and management of domains.
We do not plan to implement all features (but merge requests are welcome).

## About the Namecheap API

The Namecheap API has a single endpoint:

- `https://api.namecheap.com/xml.response`

All requests go through this one endpoint, using query strings to perform different operations.
All responses return XML data in a structured format.

There is not a 1:1 representation of XML in Elixir.
The closest thing we have is structs, but that can't discern attributes from content inside the tags.
As a result, this library creates abstractions over the XML entities returned by Namecheap, representing them as structs in a native format.

Requests are a bit easier because query params can be represented as maps.
Namecheap never requires you to send data in the request body, so we only ever need to parse XML *from* the response, never build it.

GET and POST work the same for all API operations, but the Namecheap docs [recommends using POST](https://www.namecheap.com/support/api/methods/domains/create/) for some operations, so we just use POST throughout the library.

## Installation

The package can be installed
by adding `namecheap` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:namecheap, "~> 0.2.1"}
  ]
end
```

### Configuration

You will then need to configure Namecheap for your application.

```elixir
import Config

config :namecheap,
  api_user: "example",
  api_key: "********************************",
  endpoint: "https://api.namecheap.com/xml.response"
```

#### Supported options

- `api_user` (**required**): Username of your Namecheap account.
- `api_key` (**required**): API key generated from your Namecheap account.
- `endpoint` (**required**): The endpoint to use for requests. You should set this to Namecheap's production endpoint in production, and their sandbox endpoint in development. There is no default for safety reasons.
- `user_name` (optional): Username on which each command is executed. Defaults to the value of `api_user`.
- `client_ip` (optional): [Nobody knows the purpose of this](https://www.namecheap.com/support/api/global-parameters/#comment-4726222840), but Namecheap requires it with every request. We pass the loopback address `127.0.0.1` by default, but feel free to override it.
- `parser` (optional): Any `Floki.HTMLParser` compatible parser module. Defaults to `Namecheap.Parser`.

## Usage

The `Namecheap` module contains functions for doing things in an Elixir-y way.

### Checking domains

Use `Namecheap.check_domains/1` to check the status of a list of domains.

```elixir
iex(1)> Namecheap.check_domains(["tribes.host", "us.xyz"])
{:ok,
 [
   %Namecheap.DomainCheckResult{
     available: false,
     description: "",
     domain: "tribes.host",
     eap_fee: #Decimal<0>,
     error_no: "0",
     icann_fee: #Decimal<0>,
     is_premium: false,
     premium_registration_price: #Decimal<0>,
     premium_renewal_price: #Decimal<0>,
     premium_restore_price: #Decimal<0>,
     premium_transfer_price: #Decimal<0>
   },
   %Namecheap.DomainCheckResult{
     available: true,
     description: "",
     domain: "us.xyz",
     eap_fee: #Decimal<0.0>,
     error_no: "0",
     icann_fee: #Decimal<0>,
     is_premium: true,
     premium_registration_price: #Decimal<13000.0>,
     premium_renewal_price: #Decimal<13000.0>,
     premium_restore_price: #Decimal<65.0>,
     premium_transfer_price: #Decimal<13000.0>
   }
 ]}
```

### Registering a domain

To register a domain, you'll need to construct a `Namecheap.Contact` struct, then call `Namecheap.register_domain/3`.

```elixir
iex(1)> contact = %Namecheap.Contact{first_name: "Herman", last_name: "Munster", email: "herman@munster.me", phone: "+001.1234567890", address_line_1: "1313 Mockingbird Lane", city: "Mockingbird Heights", state_or_province: "NY", postal_code: "12200", country: "United States"}
iex(2)> Namecheap.register_domain("tribes.host", 1, contact)
{
  :ok,
  %Namecheap.DomainCreateResult{
    charged_amount: #Decimal<170.1200>,
    domain: "tribes.host",
    domain_id: "670288",
    order_id: "2332723",
    realtime: true,
    registered: true,
    transaction_id: "5333932",
    whois_guard_enabled: true
  }
}
```

### Other actions

It is possible to perform all other API commands by calling `Namecheap.Client.command/3` directly.
For example:

```elixir
# Get a list of domains
Namecheap.Client.command(
  "namecheap.domains.getList",
  %{
    "ListType" => "ALL",
    "Page" => 2,
    "PageSize" => 50,
    "SortBy" => "NAME"
  },
  parser: Namecheap.NoOpParser
)
```

In this example we use the `Namecheap.NoOpParser`, which returns the raw XML response as a string.
The default parser (`Namecheap.Parser`) works too, but does not support every type of entity yet.
You can use the default parser, implement your own parser, or set the parser to `Floki` if you just want an AST.

## License

Namecheap Elixir is licensed under the MIT license.
See `LICENSE.md` for a full copy of the license.