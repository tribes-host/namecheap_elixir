defmodule Namecheap.Client do
  @moduledoc """
  Facilitates HTTP requests to the Namecheap API endpoint.
  """

  @default_parser Namecheap.Parser

  @doc """
  Runs a command against the Namecheap API, parses the response, and returns a
  `Namecheap.Response` struct.

  You should check the [Namecheap docs for each method][1] to know what
  parameters are supported. It's better to use a function from the `Namecheap`
  module instead of using the Client directly if supported.

  ## Arguments

  - `cmd` - The full name of the [Namecheap API "method"][1], eg
    `"namecheap.domains.check"`
  - `params` - Map of query paramaters to pass along with the request.
    [Global paramaters][2] are inferred from your settings, but can be
    overridden here. Example: `%{"DomainList" => "domain1.com,domain2.com"}`
  - `opts` - Keyword list of options (see below).

  ### Options

  Options supplied to the `opts` argument.

  - `parser` - Set the parser to any `Floki.HTMLParser` compatible parser.
    `Namecheap.Parser` is the default, but you can also use
    `Namecheap.NoOpParser` if you just want the raw XML, or even use `Floki`
    directly.
    
  ## Example

  ```elixir
  Namecheap.Client.command("namecheap.domains.check", %{
    "DomainList" => "domain1.com,domain2.com"
  })
  ```

  [1]: https://www.namecheap.com/support/api/methods/
  [2]: https://www.namecheap.com/support/api/global-parameters/
  """
  @spec command(cmd :: String.t(), params :: Namecheap.params(), opts :: Keyword.t()) ::
          Namecheap.Response.t()
  def command(cmd, params, opts \\ []) do
    http_client = get_http_client()
    endpoint = get_endpoint()
    parser = get_parser(opts)

    req_params =
      build_global_params()
      |> Map.put("Command", cmd)
      |> Map.merge(params)

    request = %HTTPoison.Request{
      method: :post,
      url: endpoint,
      params: req_params,
      options: [recv_timeout: 60_000]
    }

    with {:ok, %HTTPoison.Response{status_code: 200, body: body}} <- http_client.request(request),
         {:ok, %Namecheap.Response{} = response} <- parser.parse_document(body) do
      {:ok, response}
    end
  end

  defp get_http_client() do
    Application.get_env(:namecheap, :http_client, HTTPoison)
  end

  defp get_endpoint() do
    Application.get_env(:namecheap, :endpoint)
  end

  defp get_parser(opts) do
    case opts[:parser] do
      nil -> Application.get_env(:namecheap, :parser, @default_parser)
      parser when is_atom(parser) -> parser
    end
  end

  defp build_global_params() do
    api_user = Application.get_env(:namecheap, :api_user)

    %{
      "ApiUser" => api_user,
      "ApiKey" => Application.get_env(:namecheap, :api_key),
      "UserName" => Application.get_env(:namecheap, :user_name, api_user),
      "ClientIp" => Application.get_env(:namecheap, :client_ip, "127.0.0.1")
    }
  end
end
