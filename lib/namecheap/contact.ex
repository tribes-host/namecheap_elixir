defmodule Namecheap.Contact do
  @moduledoc """
  Contact/address information for a customer.
  The purpose of this module is to construct request parameters for certain
  API commands, like [`namecheap.domains.create`][1]
  and [`namecheap.domains.setContacts`][2].

  [1]: https://www.namecheap.com/support/api/methods/domains/create/
  [2]: https://www.namecheap.com/support/api/methods/domains/set-contacts/
  """
  @type t :: %Namecheap.Contact{
          organization_name: String.t() | nil,
          job_title: String.t() | nil,
          first_name: String.t(),
          last_name: String.t(),
          address_line_1: String.t(),
          address_line_2: String.t() | nil,
          city: String.t() | nil,
          state_or_province: String.t(),
          postal_code: String.t(),
          country: String.t(),
          phone: String.t(),
          phone_ext: String.t() | nil,
          fax: String.t() | nil,
          email: String.t()
        }

  defstruct organization_name: nil,
            job_title: nil,
            first_name: nil,
            last_name: nil,
            address_line_1: nil,
            address_line_2: nil,
            city: nil,
            state_or_province: nil,
            postal_code: nil,
            country: nil,
            phone: nil,
            phone_ext: nil,
            fax: nil,
            email: nil

  @doc """
  Constructs request paramaters from a `Namecheap.Contact` for use with certain
  API commands.

  `user_type` may be a string or list of strings.
  In the case of multiple strings, a single map with all types will be created.

  ## Example

  ```elixir
  iex(1)> contact = %Namecheap.Contact{first_name: "Herman", last_name: "Munster", email: "herman@munster.me", phone: "+001.1234567890", address_line_1: "1313 Mockingbird Lane", city: "Mockingbird Heights", state_or_province: "NY", postal_code: "12200", country: "United States"}
  iex(2)> Namecheap.Contact.to_params(contact, "Registrant")
  %{
    "RegistrantAddress1" => "1313 Mockingbird Lane",
    "RegistrantCity" => "Mockingbird Heights",
    "RegistrantCountry" => "United States",
    "RegistrantEmailAddress" => "herman@munster.me",
    "RegistrantFirstName" => "Herman",
    "RegistrantLastName" => "Munster",
    "RegistrantPhone" => "+001.1234567890",
    "RegistrantPostalCode" => "12200",
    "RegistrantStateProvince" => "NY"
  }
  iex(3)> Namecheap.Contact.to_params(contact, ["Registrant", "Admin"])
  %{
    "RegistrantAddress1" => "1313 Mockingbird Lane",
    "RegistrantCity" => "Mockingbird Heights",
    "RegistrantCountry" => "United States",
    "RegistrantEmailAddress" => "herman@munster.me",
    "RegistrantFirstName" => "Herman",
    "RegistrantLastName" => "Munster",
    "RegistrantPhone" => "+001.1234567890",
    "RegistrantPostalCode" => "12200",
    "RegistrantStateProvince" => "NY",
    "AdminAddress1" => "1313 Mockingbird Lane",
    "AdminCity" => "Mockingbird Heights",
    "AdminCountry" => "United States",
    "AdminEmailAddress" => "herman@munster.me",
    "AdminFirstName" => "Herman",
    "AdminLastName" => "Munster",
    "AdminPhone" => "+001.1234567890",
    "AdminPostalCode" => "12200",
    "AdminStateProvince" => "NY"
  }
  ```
  """
  @spec to_params(Namecheap.Contact.t(), user_type :: String.t() | [String.t()]) ::
          Namecheap.params()
  def to_params(%Namecheap.Contact{} = contact, user_types) when is_list(user_types) do
    Enum.reduce(user_types, %{}, fn user_type, acc ->
      Map.merge(acc, to_params(contact, user_type))
    end)
  end

  def to_params(%Namecheap.Contact{} = contact, user_type) when is_binary(user_type) do
    user_type = Macro.camelize(user_type)

    %{
      "OrganizationName" => contact.organization_name,
      "JobTitle" => contact.job_title,
      "FirstName" => contact.first_name,
      "LastName" => contact.last_name,
      "Address1" => contact.address_line_1,
      "Address2" => contact.address_line_2,
      "City" => contact.city,
      "StateProvince" => contact.state_or_province,
      "PostalCode" => contact.postal_code,
      "Country" => contact.country,
      "Phone" => contact.phone,
      "PhoneExt" => contact.phone_ext,
      "Fax" => contact.fax,
      "EmailAddress" => contact.email
    }
    |> Enum.map(fn {k, v} -> {user_type <> k, v} end)
    |> Enum.reject(fn {_, v} -> is_nil(v) end)
    |> Map.new()
  end
end
