import Config

config :namecheap,
  http_client: Namecheap.HTTPoisonMock,
  endpoint: "https://api.namecheap.test/xml.response"
