defmodule Namecheap.Error do
  @moduledoc """
  Represents a Namecheap API error.
  See the [full list of error codes][1] for more information.

  ```elixir
  %Namecheap.Error{
    error_code: "1010104",
    message: "Parameter Command is missing"
  }
  ```

  [1]: https://www.namecheap.com/support/api/error-codes/
  """
  @type t :: %__MODULE__{
          error_code: String.t(),
          message: String.t()
        }

  defstruct error_code: "", message: ""
end
