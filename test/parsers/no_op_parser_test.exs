defmodule Namecheap.NoOpParserTest do
  use ExUnit.Case
  alias Namecheap.NoOpParser

  test "parse_document/1 returns the original string without modification" do
    body = """
    <?xml version="1.0" encoding="utf-8"?>
    <ApiResponse Status="OK" xmlns="http://api.namecheap.com/xml.response">
      <Errors />
      <Warnings />
      <RequestedCommand>namecheap.domains.check</RequestedCommand>
      <CommandResponse Type="namecheap.domains.check">
        <DomainCheckResult Domain="tribes.host" Available="true" ErrorNo="0" Description="" IsPremiumName="false" PremiumRegistrationPrice="0" PremiumRenewalPrice="0" PremiumRestorePrice="0" PremiumTransferPrice="0" IcannFee="0" EapFee="0" />
      </CommandResponse>
      <Server>PHX01SBAPIEXT06</Server>
      <GMTTimeDifference>--5:00</GMTTimeDifference>
      <ExecutionTime>1.067</ExecutionTime>
    </ApiResponse>\
    """

    assert NoOpParser.parse_document(body) == {:ok, body}
  end
end
