defmodule NamecheapTest do
  use ExUnit.Case
  alias Namecheap.DomainCheckResult

  test "check_domains/1 returns a list of %DomainCheckResult{} structs" do
    domains = ["tribes.host", "arstneio.xyz"]

    assert {:ok,
            [
              %DomainCheckResult{domain: "arstneio.xyz"},
              %DomainCheckResult{domain: "tribes.host"}
            ]} = Namecheap.check_domains(domains)
  end

  test "register_domain/3 returns a %DomainCreateResult{} struct" do
    contact = %Namecheap.Contact{
      first_name: "Herman",
      last_name: "Munster",
      email: "herman@munster.me",
      phone: "+001.1234567890",
      address_line_1: "1313 Mockingbird Lane",
      city: "Mockingbird Heights",
      state_or_province: "NY",
      postal_code: "12200",
      country: "United States"
    }

    expected = %Namecheap.DomainCreateResult{
      charged_amount: Decimal.new("170.1200"),
      domain: "tribes.host",
      domain_id: "670288",
      order_id: "2332723",
      realtime: true,
      registered: true,
      transaction_id: "5333932",
      whois_guard_enabled: true
    }

    result = Namecheap.register_domain("tribes.host", 1, contact)

    assert {:ok, expected} == result
  end
end
