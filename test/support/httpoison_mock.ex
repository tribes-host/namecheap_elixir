defmodule Namecheap.HTTPoisonMock do
  def request(%HTTPoison.Request{
        params: %{
          "Command" => "namecheap.domains.check",
          "DomainList" => "tribes.host,arstneio.xyz"
        }
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/namecheap.domains.check.xml"),
       status_code: 200
     }}
  end

  def request(%HTTPoison.Request{
        params: %{"Command" => "namecheap.domains.create", "DomainName" => "tribes.host"}
      }) do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/namecheap.domains.create.xml"),
       status_code: 200
     }}
  end
end
