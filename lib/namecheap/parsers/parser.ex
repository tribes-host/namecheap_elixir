defmodule Namecheap.Parser do
  @moduledoc """
  Converts Namecheap API responses into structs of our native format.
  This is the default parser.
  """
  @behaviour Floki.HTMLParser

  @doc """
  Takes raw XML data from a Namecheap response and converts it into
  a `Namecheap.Response` struct.
  """
  @impl true
  @spec parse_document(body :: String.t()) :: {:ok, Namecheap.Response.t()}
  def parse_document(body) when is_binary(body) do
    with {:ok, ast} <- to_ast(body),
         %Namecheap.Response{} = response <- to_namecheap_struct(ast) do
      {:ok, response}
    end
  end

  @impl true
  def parse_fragment(body), do: parse_document(body)

  defp to_ast(body) when is_binary(body) do
    Floki.parse_document(body)
  end

  defp to_namecheap_struct(ast) when is_list(ast) do
    ast
    |> Enum.find(fn node -> match?({"apiresponse", _, _}, node) end)
    |> parse_entity()
  end

  @doc """
  Attempts to parse a single node into a native struct.
  Returns a native struct, or `{:unimplemented, node}` on failure.
  All element names and attributes must be lowercase.

  ## Example

  ```elixir
  iex(1)> Namecheap.Parser.parse_entity({"domaincheckresult", [{"domain", "tribes.host"}], []})
  %Namecheap.DomainCheckResult{domain: "tribes.host", ...}
  ```
  """
  @spec parse_entity(node :: Floki.html_node()) :: struct() | {:unimplemented, Floki.html_node()}
  def parse_entity({"apiresponse", attrs, content}) do
    attrs = Map.new(attrs)

    # Root elements don't have attributes that we care about
    # so we can map their names to their content.
    elems = Enum.reduce(content, %{}, fn {name, _, val}, acc -> Map.put(acc, name, val) end)

    %Namecheap.Response{
      status: attrs["status"],
      errors: elems["errors"] |> Enum.map(&parse_entity/1),
      warnings: elems["warnings"] |> Enum.map(&parse_entity/1),
      command: elems["requestedcommand"] |> Enum.at(0),
      server: elems["server"] |> Enum.at(0),
      gmt_time_difference: elems["gmttimedifference"] |> Enum.at(0),
      execution_time: elems["executiontime"] |> Enum.at(0) |> Decimal.new(),
      body: elems["commandresponse"] |> Enum.map(&parse_entity/1)
    }
  end

  def parse_entity({"domaincheckresult", attrs, _}) do
    attrs = Map.new(attrs)

    %Namecheap.DomainCheckResult{
      domain: attrs["domain"],
      available: attrs["available"] == "true",
      error_no: attrs["errorno"],
      description: attrs["description"],
      is_premium: attrs["ispremiumname"] == "true",
      premium_registration_price: attrs["premiumregistrationprice"] |> Decimal.new(),
      premium_renewal_price: attrs["premiumrenewalprice"] |> Decimal.new(),
      premium_restore_price: attrs["premiumrestoreprice"] |> Decimal.new(),
      premium_transfer_price: attrs["premiumtransferprice"] |> Decimal.new(),
      icann_fee: attrs["icannfee"] |> Decimal.new(),
      eap_fee: attrs["eapfee"] |> Decimal.new()
    }
  end

  def parse_entity({"domaincreateresult", attrs, _}) do
    attrs = Map.new(attrs)

    %Namecheap.DomainCreateResult{
      domain: attrs["domain"],
      registered: attrs["registered"] == "true",
      charged_amount: attrs["chargedamount"] |> Decimal.new(),
      domain_id: attrs["domainid"],
      order_id: attrs["orderid"],
      transaction_id: attrs["transactionid"],
      whois_guard_enabled: attrs["whoisguardenable"] == "true",
      realtime: attrs["nonrealtimedomain"] != "true"
    }
  end

  def parse_entity({"error", attrs, [content]}) when is_binary(content) do
    attrs = Map.new(attrs)

    %Namecheap.Error{
      error_code: attrs["number"],
      message: content
    }
  end

  def parse_entity(node) do
    {:unimplemented, node}
  end
end
