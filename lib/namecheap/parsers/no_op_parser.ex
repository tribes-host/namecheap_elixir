defmodule Namecheap.NoOpParser do
  @moduledoc """
  Parser that returns the raw XML data from the response as an unmodified
  string.

  ```elixir
  iex(1)> Namecheap.Client.command("namecheap.domains.check", %{"DomainList" => "tribes.host"}, parser: Namecheap.NoOpParser)
  {:ok,
   "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<ApiResponse Status=\"OK\" xmlns=\"http://api.namecheap.com/xml.response\">\r\n  <Errors />\r\n  <Warnings />\r\n  <RequestedCommand>namecheap.domains.check</RequestedCommand>\r\n  <CommandResponse Type=\"namecheap.domains.check\">\r\n    <DomainCheckResult Domain=\"tribes.host\" Available=\"true\" ErrorNo=\"0\" Description=\"\" IsPremiumName=\"false\" PremiumRegistrationPrice=\"0\" PremiumRenewalPrice=\"0\" PremiumRestorePrice=\"0\" PremiumTransferPrice=\"0\" IcannFee=\"0\" EapFee=\"0\" />\r\n  </CommandResponse>\r\n  <Server>PHX01SBAPIEXT05</Server>\r\n  <GMTTimeDifference>--5:00</GMTTimeDifference>\r\n  <ExecutionTime>1.315</ExecutionTime>\r\n</ApiResponse>"}
  ```
  """
  @behaviour Floki.HTMLParser

  @impl true
  @spec parse_document(body :: String.t()) :: {:ok, String.t()}
  def parse_document(body) when is_binary(body), do: {:ok, body}

  @impl true
  def parse_fragment(node), do: {:ok, node}
end
