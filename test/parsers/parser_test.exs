defmodule Namecheap.ParserTest do
  use ExUnit.Case

  test "parse_entity/1 parses an Error entity" do
    entity = {"error", [{"number", "4"}], ["Parameter Command is Invalid"]}
    expected = %Namecheap.Error{error_code: "4", message: "Parameter Command is Invalid"}
    assert Namecheap.Parser.parse_entity(entity) == expected
  end
end
