defmodule Namecheap.ContactTest do
  use ExUnit.Case

  describe "to_params/2" do
    setup do
      contact = %Namecheap.Contact{
        first_name: "Herman",
        last_name: "Munster",
        email: "herman@munster.me",
        phone: "+001.1234567890",
        address_line_1: "1313 Mockingbird Lane",
        city: "Mockingbird Heights",
        state_or_province: "NY",
        postal_code: "12200",
        country: "United States"
      }

      %{contact: contact}
    end

    test "converts the contact to valid request params", %{contact: contact} do
      expected = %{
        "RegistrantAddress1" => "1313 Mockingbird Lane",
        "RegistrantCity" => "Mockingbird Heights",
        "RegistrantCountry" => "United States",
        "RegistrantEmailAddress" => "herman@munster.me",
        "RegistrantFirstName" => "Herman",
        "RegistrantLastName" => "Munster",
        "RegistrantPhone" => "+001.1234567890",
        "RegistrantPostalCode" => "12200",
        "RegistrantStateProvince" => "NY"
      }

      assert Namecheap.Contact.to_params(contact, "registrant") == expected
    end

    test "supports multiple user_types", %{contact: contact} do
      result =
        Namecheap.Contact.to_params(contact, ["registrant", "tech", "admin", "aux_billing"])

      assert %{
               "RegistrantFirstName" => "Herman",
               "TechFirstName" => "Herman",
               "AdminFirstName" => "Herman",
               "AuxBillingFirstName" => "Herman"
             } = result
    end
  end
end
