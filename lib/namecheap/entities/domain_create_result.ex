defmodule Namecheap.DomainCreateResult do
  @moduledoc """
  Represents entities returned by the [`namecheap.domains.create`][1] command.

  ```elixir
  %Namecheap.DomainCreateResult{
    charged_amount: #Decimal<170.1200>,
    domain: "tribes.host",
    domain_id: "670288",
    order_id: "2332723",
    realtime: true,
    registered: true,
    transaction_id: "5333932",
    whois_guard_enabled: false
  }
  ```

  [1]: https://www.namecheap.com/support/api/methods/domains/create/
  """
  @type t :: %__MODULE__{
          domain: String.t(),
          registered: boolean(),
          charged_amount: Decimal.t(),
          domain_id: String.t(),
          order_id: String.t(),
          transaction_id: String.t(),
          whois_guard_enabled: boolean(),
          realtime: boolean()
        }

  defstruct domain: "",
            registered: false,
            charged_amount: Decimal.new(0),
            domain_id: "",
            order_id: "",
            transaction_id: "",
            whois_guard_enabled: false,
            realtime: true
end
