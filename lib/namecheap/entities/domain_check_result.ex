defmodule Namecheap.DomainCheckResult do
  @moduledoc """
  Represents entities returned by the [`namecheap.domains.check`][1] command.

  ```elixir
  %Namecheap.DomainCheckResult{
    available: true,
    description: "",
    domain: "tribes.host",
    eap_fee: #Decimal<0>,
    error_no: "0",
    icann_fee: #Decimal<0>,
    is_premium: false,
    premium_registration_price: #Decimal<0>,
    premium_renewal_price: #Decimal<0>,
    premium_restore_price: #Decimal<0>,
    premium_transfer_price: #Decimal<0>
  }
  ```

  [1]: https://www.namecheap.com/support/api/methods/domains/check/
  """
  @type t :: %__MODULE__{
          domain: String.t(),
          available: boolean(),
          error_no: String.t(),
          description: String.t(),
          is_premium: boolean(),
          premium_registration_price: Decimal.t(),
          premium_renewal_price: Decimal.t(),
          premium_restore_price: Decimal.t(),
          premium_transfer_price: Decimal.t(),
          icann_fee: Decimal.t(),
          eap_fee: Decimal.t()
        }

  defstruct domain: "",
            available: false,
            error_no: "",
            description: "",
            is_premium: false,
            premium_registration_price: Decimal.new(0),
            premium_renewal_price: Decimal.new(0),
            premium_restore_price: Decimal.new(0),
            premium_transfer_price: Decimal.new(0),
            icann_fee: Decimal.new(0),
            eap_fee: Decimal.new(0)
end
