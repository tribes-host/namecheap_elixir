defmodule Namecheap.MixProject do
  use Mix.Project

  def project do
    [
      app: :namecheap,
      version: "0.2.1",
      elixir: "~> 1.11",
      name: "Namecheap",
      description: "Client library for interacting with the Namecheap API.",
      source_url: "https://gitlab.com/tribes-host/namecheap_elixir",
      package: package(),
      docs: docs(),
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/tribes-host/namecheap_elixir"}
    ]
  end

  def docs do
    [
      main: "readme",
      extras: [
        "README.md",
        "LICENSE.md"
      ],
      groups_for_modules: groups_for_modules()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:floki, "~> 0.30.0"},
      {:httpoison, "~> 1.8"},
      {:ex_doc, "~> 0.22", only: :dev, runtime: false},
      {:decimal, "~> 2.0"}
    ]
  end

  defp groups_for_modules do
    [
      API: [
        Namecheap.Client,
        Namecheap.Response,
        Namecheap.Contact
      ],
      Parsers: [
        Namecheap.Parser,
        Namecheap.NoOpParser
      ],
      Entities: [
        Namecheap.DomainCheckResult,
        Namecheap.DomainCreateResult,
        Namecheap.Error
      ]
    ]
  end
end
