defmodule Namecheap.ClientTest do
  use ExUnit.Case

  test "command/1 returns a valid %Namecheap.Response{} struct" do
    response =
      Namecheap.Client.command("namecheap.domains.check", %{
        "DomainList" => "tribes.host,arstneio.xyz"
      })

    expected =
      {:ok,
       %Namecheap.Response{
         body: [
           %Namecheap.DomainCheckResult{
             available: true,
             description: "",
             domain: "arstneio.xyz",
             eap_fee: Decimal.new("0.0"),
             error_no: "0",
             icann_fee: Decimal.new("0"),
             is_premium: false,
             premium_registration_price: Decimal.new("0"),
             premium_renewal_price: Decimal.new("0"),
             premium_restore_price: Decimal.new("0"),
             premium_transfer_price: Decimal.new("0")
           },
           %Namecheap.DomainCheckResult{
             available: true,
             description: "",
             domain: "tribes.host",
             eap_fee: Decimal.new("0"),
             error_no: "0",
             icann_fee: Decimal.new("0"),
             is_premium: false,
             premium_registration_price: Decimal.new("0"),
             premium_renewal_price: Decimal.new("0"),
             premium_restore_price: Decimal.new("0"),
             premium_transfer_price: Decimal.new("0")
           }
         ],
         command: "namecheap.domains.check",
         errors: [],
         execution_time: Decimal.new("1.302"),
         gmt_time_difference: "--5:00",
         server: "PHX01SBAPIEXT06",
         status: "OK",
         warnings: []
       }}

    assert expected == response
  end
end
